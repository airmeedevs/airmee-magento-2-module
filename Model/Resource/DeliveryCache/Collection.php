<?php

namespace Airmee\Shipping\Model\Resource\DeliveryCache;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Airmee\Shipping\Model\Resource\DeliveryCache
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Airmee\Shipping\Model\DeliveryCache',
            'Airmee\Shipping\Model\Resource\DeliveryCache'
        );
    }
}
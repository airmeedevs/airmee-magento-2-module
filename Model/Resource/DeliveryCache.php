<?php

namespace Airmee\Shipping\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class DeliveryCache
 * @package Airmee\Shipping\Model\Resource
 */
class DeliveryCache extends AbstractDb
{
    /**
     * Define primary table
     */
    protected function _construct()
    {
        $this->_init('airmee_shipping_delivery_cache', 'id');
    }
}

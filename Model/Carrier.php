<?php
namespace Airmee\Shipping\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Config;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Directory\Model\CountryFactory;
use Psr\Log\LoggerInterface;
use Airmee\Shipping\Model\Resource\DeliveryCache\CollectionFactory;
use Airmee\Shipping\Model\AirmeeClient;
use Airmee\Shipping\Model\DeliveryCacheFactory;
use Airmee\PhpSdk\Core\Exceptions\UnknownPlaceException;
use Airmee\PhpSdk\Core\Exceptions\ServerErrorException;

/**
 * Class Carrier
 * @package Airmee\Shipping\Model
 */
class Carrier extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'airmee';

    /**
     * Whether this carrier has fixed rates calculation
     *
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var AirmeeClient
     */
    protected $airmeeClient;

    /**
     * @var \Airmee\Shipping\Model\DeliveryCacheFactory
     */
    protected $deliveryCacheFactory;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;


    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param DeliveryCacheFactory $deliveryCacheFactory
     * @param CountryFactory $countryFactory
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        AirmeeClient $airmeeClient,
        DeliveryCacheFactory $deliveryCacheFactory,
        CountryFactory $countryFactory,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->airmeeClient = $airmeeClient;
        $this->deliveryCacheFactory = $deliveryCacheFactory;
        $this->countryFactory = $countryFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Generates list of allowed carrier`s shipping methods
     * Displays on cart price rules page
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => __($this->getConfigData('name'))];
    }

    /**
     * Collect and get rates for each delivery option
     *
     * @param RateRequest $request
     * @return DataObject|bool|null
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        // Check if Airmee shipping is enabled.
        if (!$this->isActive()) {
            return false;
        }

        // Check if the item metrics allow it to be shipped by Airmee.
        if(!$this->verifyDeliverableMetrics($request->getAllItems())) {
            return false;
        }

        /*
         * Collect rates for each delivery option (time slot).
         */
        $result = $this->rateResultFactory->create();
        $postcode = str_replace(' ', '', $request->getData('dest_postcode'));
        $countryCode = $request->getData('dest_country_id');
        $countryName = $this->countryFactory->create()->loadByCode($countryCode)->getName();
        try {
            if($request->getData('dest_street') && $request->getData('dest_city')) {
                $deliveryOptions = $this->getDeliveryOptions($postcode, $countryName, $request->getData('dest_street'), $request->getData('dest_city'));
            } else {
                $deliveryOptions = $this->getDeliveryOptions($postcode, $countryName);
            }
            if($deliveryOptions) {
                foreach ($deliveryOptions as $deliveryOption) {
                    $deliveryOptionId = $this->cacheDeliveryOption($deliveryOption, $postcode);
                    $method = $this->buildRateForDeliveryOption($deliveryOptionId, $deliveryOption);
                    $result->append($method);
                }
            }
        } catch(ServerErrorException $e) {
            if(substr($e->getMessage(), 0, 26) == 'Address could not be found') {
                // @TODO: In the future we'll want to use messageManager. For now we have to set headers and die to work around a core bug.
                //$this->messageManager->addWarning( __('Airmee shipping is unable to find your address. Please verify that it is entered correctly.'));

                header('X-PHP-Response-Code: 404', true, 404);
                header('Content-Type: application/json');
                die('{"message": "Airmee shipping is unable to find your address. Please verify that it is entered correctly.", "parameters": []}');
            }
        }

        return $result;
    }

    /**
     * Verify the items in the cart are shippable via Airmee.
     *
     * @param $items
     * @return bool
     */
    protected function verifyDeliverableMetrics($items) {
        $maxSizeItem = $this->airmeeClient->getClient()->productThresholdForPlace($this->airmeeClient->getPlaceId());
        foreach($items as $item) {
            // $maxSizeItem has methods for getLength(), getWidth() and getHeight() which can be used for additional checks.
            if($maxSizeItem->getWeight() > $item->getWeight()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Cache a delivery option in the database so we can reference it by an internal ID later.
     *
     * @param \Airmee\PhpSdk\Core\Models\Schedule $deliveryOption Delivery option info
     * @return int
     */
    protected function cacheDeliveryOption(\Airmee\PhpSdk\Core\Models\Schedule $deliveryOption, $postcode) {
        // Find matching existing cached delivery option if one exists.
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('postcode', ['eq' => $postcode])
            ->addFieldToFilter('pickup_start_timestamp', ['eq' => $deliveryOption->getPickup()->getStart()->format('U')])
            ->addFieldToFilter('pickup_end_timestamp', ['eq' => $deliveryOption->getPickup()->getEnd()->format('U')])
            ->addFieldToFilter('delivery_start_timestamp', ['eq' => $deliveryOption->getDropoff()->getStart()->format('U')])
            ->addFieldToFilter('delivery_end_timestamp', ['eq' => $deliveryOption->getDropoff()->getEnd()->format('U')])
            ->setPageSize(1)->setCurPage(1)->load();

        // There is only a maximum of one matching cache due to limits, but a loop is the cleanest way to access it.
        $existingCache = false;
        foreach($collection as $item) {
            $existingCache = $item;
        }

        // If a match exists extend the expiry time and return the ID
        if($existingCache) {
            $existingCache->setOptionExpiry(time() + 3600);
            $existingCache->save();
            return $existingCache->getId();
        }

        // Since no match exists we need to create a new cache entry.
        $deliveryCache = $this->deliveryCacheFactory->create();
        $deliveryCache->setData('postcode', $postcode)
            ->setData('pickup_start_timestamp', $deliveryOption->getPickup()->getStart()->format('U'))
            ->setData('pickup_end_timestamp', $deliveryOption->getPickup()->getEnd()->format('U'))
            ->setData('delivery_start_timestamp', $deliveryOption->getDropoff()->getStart()->format('U'))
            ->setData('delivery_end_timestamp', $deliveryOption->getDropoff()->getEnd()->format('U'))
            ->setData('option_expiry', time() + 3600)
            ->save();
        return $deliveryCache->getId();
    }

    /**
     * Build Rate based on delivery option data
     *
     * @param string $deliveryOptionId Shipping method identifier
     * @param \Airmee\PhpSdk\Core\Models\Schedule $deliveryOption Delivery option info
     * @return Method
     */
    protected function buildRateForDeliveryOption($deliveryOptionId, \Airmee\PhpSdk\Core\Models\Schedule $deliveryOption)
    {
        $rateResultMethod = $this->rateMethodFactory->create();

        /**
         * Carrier is the section options are shown under ('Airmee Shipping' or similar, as defined in the admin)
         */
        $rateResultMethod->setData('carrier', $this->getCarrierCode());
        $rateResultMethod->setData('carrier_title', $this->getConfigData('title'));

        /**
         * Method is the specific delivery option (time slot)
         */
        $rateResultMethod->setData('method_title', $deliveryOption->getDropoff()->getFormatted());
        $rateResultMethod->setData('method', $deliveryOptionId);

        /**
         * Pricing, specific to the delivery option.
         */
        $rateResultMethod->setPrice($this->getConfigData('price'));
        $rateResultMethod->setData('cost', $this->getConfigData('price'));

        return $rateResultMethod;
    }


    /**
     * Get available delivery options from Airmee API
     *
     * @param $postcode
     * @param $country
     * @param string|null $streetAndNumber Optional. If specified $city must also be specified.
     * @param string|null $city Optional. If specified $streetAndNumber must also be specified.
     * @return array|bool
     */
    protected function getDeliveryOptions($postcode, $country, $streetAndNumber = null, $city = null)
    {
        if($postcode == false || strlen($postcode) < 1) {
            return false;
        }
        try {
            if($streetAndNumber && $city) {
                $destination = new \Airmee\PhpSdk\Core\Models\Address($postcode, $country, $streetAndNumber, $city);
            } else {
                $destination = new \Airmee\PhpSdk\Core\Models\Address($postcode, $country);
            }
            return $this->airmeeClient->getClient()->deliveryIntervalsForAddress($this->airmeeClient->getPlaceId(), $destination);
        } catch (UnknownPlaceException $e) {
            return array();
        }
    }
}

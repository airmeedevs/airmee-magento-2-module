<?php
namespace Airmee\Shipping\Model\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Shipping\Model\ShipmentNotifierFactory;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Model\Convert\OrderFactory;
use Magento\Directory\Model\CountryFactory;
use Airmee\Shipping\Model\AirmeeClient;
use Airmee\Shipping\Model\DeliveryCacheFactory;
use Airmee\PhpSdk\Core\Models\Recipient;
use Airmee\PhpSdk\Core\Models\Address;
use libphonenumber\PhoneNumberUtil;
use Airmee\PhpSdk\Core\Models\TimeRange;
use Airmee\PhpSdk\Core\Models\Item;

/**
 * Class CheckoutCompleteObserver
 * @package Airmee\Shipping\Observer
 */
class CheckoutCompleteObserver implements ObserverInterface {

    /**
     * @var AirmeeClient
     */
    protected $airmeeClient;

    /**
     * @var DeliveryCacheFactory
     */
    protected $deliveryCacheFactory;

    /**
     * @var ShipmentNotifierFactory
     */
    protected $shipmentNotifierFactory;

    /**
     * @var TrackFactory
     */
    protected $trackFactory;

    /**
     * @var OrderFactory
     */
    protected $convertOrderFactory;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * CheckoutCompleteObserver constructor.
     * @param AirmeeClient $airmeeClient
     * @param DeliveryCacheFactory $deliveryCacheFactory
     */
    public function __construct(
        AirmeeClient $airmeeClient,
        DeliveryCacheFactory $deliveryCacheFactory,
        ShipmentNotifierFactory $shipmentNotifierFactory,
        TrackFactory $trackFactory,
        OrderFactory $convertOrderFactory,
        CountryFactory $countryFactory
    ) {
        $this->airmeeClient = $airmeeClient;
        $this->deliveryCacheFactory = $deliveryCacheFactory;
        $this->shipmentNotifierFactory = $shipmentNotifierFactory;
        $this->trackFactory = $trackFactory;
        $this->convertOrderFactory = $convertOrderFactory;
        $this->countryFactory = $countryFactory;
    }

        /**
     * fires when sales_order_place_after is dispatched
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer) {
        $order = $observer->getEvent()->getOrder();
        if(substr($order->getShippingMethod(), 0, 6) == 'airmee') {
            $shippingMethod = explode('_', $order->getShippingMethod(), 2);
            $airmeeCacheId = $shippingMethod[1];

            $deliveryCache = $this->deliveryCacheFactory->create()->load($airmeeCacheId);
            $country = $this->countryFactory->create()->loadByCode($order->getShippingAddress()->getCountryId())->getName();

            $recipient = new Recipient($order->getCustomerName(), PhoneNumberUtil::getInstance()->parse($order->getShippingAddress()->getTelephone(), $order->getShippingAddress()->getCountryId()), $order->getCustomerEmail());
            $address = new Address($order->getShippingAddress()->getPostcode(), $country, $order->getShippingAddress()->getStreet(1), $order->getShippingAddress()->getCity());

            $pickupInterval = new TimeRange($deliveryCache->getPickupStartTimestamp(), $deliveryCache->getPickupEndTimestamp());
            $dropoffInterval = new TimeRange($deliveryCache->getDeliveryStartTimestamp(), $deliveryCache->getDeliveryEndTimestamp());

            $items = array();
            foreach($order->getAllItems() as $item) {
                if(!$item->getQtyToShip() || $item->getIsVirtual()) {
                    continue;
                }
                $money = new \Money\Money($item->getPrice(), new \Money\Currency($order->getOrderCurrencyCode()));
                array_push($items, new Item(1, 1, 1, doubleval($item->getWeight()), $money, $item->getSku(), $item->getQty()));
            }

            $delivery = $this->airmeeClient->getClient()->requestDelivery(
                $this->airmeeClient->getPlaceId(),
                $order->getIncrementId(),
                $recipient,
                $address,
                $items,
                $pickupInterval,
                $dropoffInterval
            );


            $this->createShipment($order, $delivery->getTrackingUrl());
        }

    }

    protected function createShipment($order, $tracking) {
        $convertOrder = $this->convertOrderFactory->create();
        $shipment = $convertOrder->toShipment($order);
        $package = array();

        $order->save();

        $packaging = array(
            'items' => array()
        );

        $totalWeight = 0;

        foreach ($order->getAllItems() as $orderItem) {
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $qtyShipped = $orderItem->getQtyToShip();
            $shipmentItem = $convertOrder->itemToShipmentItem($orderItem);
            $shipmentItem->setQty($qtyShipped);
            $shipmentItem->setOrderItemId($orderItem->getId());
            $shipmentItem->setOrderId($order->getId());

            $packaging['items'][$shipmentItem->getOrderItemId()] = array(
                'qty' => $shipmentItem->getQty(),
                'custom_value' => $shipmentItem->getPrice(),
                'price' => $shipmentItem->getPrice(),
                'name' => $shipmentItem->getName(),
                'weight' => $shipmentItem->getWeight(),
                'product_id' => $shipmentItem->getProductId(),
                'order_item_id' => $shipmentItem->getOrderItemId()
            );
            $totalWeight += $shipmentItem->getItem();

            $shipment->addItem($shipmentItem);

        }

        $packaging['params'] = array(
            'container' => '',
            'weight' => $totalWeight,
            'length' => 1,
            'width' => 1,
            'height' => 1,
            'weight_units' => 'KILOGRAM',
            'dimension_units' => 'CENTIMETER',
            'content_type' => '',
            'content_type_other' => ''
        );
        $package[] = $packaging;

        $shipment->setPackages($package);
        $shipment->register();
        $order = $shipment->getOrder();
        $order->setIsInProcess(true);
        $order->save();
        $shipment->save();

        $data = array(
            'carrier_code' => 'custom',
            'title' => 'Airmee Tracking',
            'number' => $tracking,
        );
        $track = $this->trackFactory->create();
        $track->addData($data);
        $track->setShipment($shipment);
        $track->setOrderId($order->getId());
        $track->save();

        $shipment->addTrack($track);

        $this->shipmentNotifierFactory->create()->notify($shipment);

        $shipment->save();

    }
}

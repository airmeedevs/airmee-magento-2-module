<?php
namespace Airmee\Shipping\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Wrapper for loading the Airmee API Client and settings so that it can be loaded in via DI.
 *
 * Class AirmeeClient
 * @package Airmee\Shipping\Model
 */
class AirmeeClient {

    /**
     * Airmee API Client
     *
     * @var \Airmee\PhpSdk\Core\AirmeeApi
     */
    protected $client;

    /**
     * Config settings
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * AirmeeClient constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
        $authToken = $this->scopeConfig->getValue('carriers/airmee/api_jwt_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $testMode = $this->scopeConfig->getValue('carriers/airmee/test_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $config = array(
            'auth.jwt' => $authToken,
            'sandbox' => $testMode
        );

        // We can't load the API client via DI due to the config params needing to be passed in.
        $this->client = new \Airmee\PhpSdk\Core\AirmeeApi($config);
    }

    /**
     * Retrieve the API Client.
     *
     * @return \Airmee\PhpSdk\Core\AirmeeApi
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Retrieve the Place ID for use with the API Client.
     *
     * @return string Place ID
     */
    public function getPlaceId() {
        return $this->scopeConfig->getValue('carriers/airmee/api_place_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
<?php
namespace Airmee\Shipping\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class DeliveryCache
 * @package Airmee\Shipping\Model
 */
class DeliveryCache extends AbstractModel
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Airmee\Shipping\Model\Resource\DeliveryCache');
    }
}

<?php
namespace Airmee\Shipping\Cron;

use Magento\Framework\App\ObjectManager;

/**
 * Class CacheClear
 * @package Airmee\Shipping\Cron
 */
class CacheClear {
    /**
     * @var ObjectManager
     */
    protected $_objectManager;

    public function __construct(ObjectManager $objectManager) {
        $this->_objectManager = $objectManager;
    }

    /**
     * Run the cron task (clear the delivery cache)
     */
    public function execute() {
        $collection = $this->_objectManager->create('Airmee\Shipping\Model\ResourceModel\DeliveryCache\Collection');
        $collection->addFieldToFilter('option_expiry', ['gt' => time()]);
        $collection->load();
        foreach($collection as $item) {
            $collection->getEntity()->delete($item);
        }
    }
}
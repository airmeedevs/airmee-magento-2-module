## Overview

Airmee Magento 2 Shipping Module

## Installation

Update the root composer.json for Magento 2 to add a dependency on this package.

This can be done by adding the following to the 'require' section.

    "airmee/magento-2-module": "1.0.0"

Once installed, run the following commands to enable the module and run it's database migrations.

    bin/magento module:enable Airmee_Shipping
    bin/magento setup:upgrade

## Configuration

This module can be configured within the Store Configuration's Shipping Methods section.

If you don't have your JWT Token and Store ID you'll need to reach out Airmee support.

## Product Dimensions

This module will hide it's shipping methods if the package is too heavy.

If you need to restrict by size modify the following:

    Airmee\Shipping\Model\Carrier:verifyDeliverableMetrics

 * Add logic to compare your product's custom attributes against the measurements already available in the SDK response.

    Airmee\Shipping\Observer:execute

 * Adjust the creation of Item objects to include actual dimensions.

## Compatibility

This module is compatible with Magento 2.0.x and 2.1.x.

This module has full compatibility with the supported versions of PHP for Magento 2.0 and 2.1.

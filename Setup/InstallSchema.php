<?php

namespace Airmee\Shipping\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class InstallSchema
 * @package Airmee\Shipping\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * Install the module, set up the cache table so we can reference delivery options (time slots) by id.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('airmee_shipping_delivery_cache')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Cache ID'
        )->addColumn(
            'postcode',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            [],
            'Postcode / Zipcode'
        )->addColumn(
            'pickup_start_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Pickup Start Timestamp'
        )->addColumn(
            'pickup_end_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Pickup End Timestamp'
        )->addColumn(
            'delivery_start_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Delivery Start Timestamp'
        )->addColumn(
            'delivery_end_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Delivery End Timestamp'
        )->addColumn(
            'option_expiry',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Delivery Option Expiry Time'
        )->setComment(
            'Cache of shipping options'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}

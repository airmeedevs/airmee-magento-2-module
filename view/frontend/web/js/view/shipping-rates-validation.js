/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        '../model/shipping-rates-validator',
        '../model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        airmeeShippingRatesValidator,
        airmeeShippingRatesValidationRules
    ) {
        "use strict";
        defaultShippingRatesValidator.registerValidator('airmee', airmeeShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('airmee', airmeeShippingRatesValidationRules);
        return Component;
    }
);

<?php
namespace Airmee\Shipping\Test\Unit\Model;

use Airmee\Shipping\Model\Carrier;

/**
 * Class CarrierTest
 * @package Airmee\Shipping\Test\Unit\Model
 *
 * @TODO: Test collectRates and it's dependencies by mocking API calls and their responses
 */
class CarrierTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Carrier
     */
    protected $carrier;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Shipping\Model\Rate\Result|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateResultFactory;

    /**
     * @var \Magento\Shipping\Model\Rate\Result|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateResult;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\Method|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateMethodFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateErrorFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\Method|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateResultMethod;

    /**
     * @var \Psr\Log\LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $logger;

    /**
     * @var \Airmee\Shipping\Model\AirmeeClient|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $airmeeClient;

    /**
     * @var \Airmee\Shipping\Model\DeliveryCacheFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $deliveryCacheFactory;

    /**
     * @var \Magento\Directory\Model\CountryFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $countryFactory;

    /**
     * @var \Airmee\Shipping\Model\Resource\DeliveryCache\CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collectionFactory;


    /**
     * Set up for tests
     */
    protected function setUp() {
        $this->scopeConfig = $this->getMockBuilder('Magento\Framework\App\Config\ScopeConfigInterface')
            ->getMockForAbstractClass();
        $this->rateErrorFactory = $this->getMockBuilder('Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->logger = $this->getMockBuilder('Psr\Log\LoggerInterface')
            ->getMockForAbstractClass();
        $this->rateResultFactory = $this->getMockBuilder('Magento\Shipping\Model\Rate\ResultFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->rateResult = $this->getMockBuilder('Magento\Shipping\Model\Rate\Result')
            ->disableOriginalConstructor()
            ->getMock();
        $this->rateMethodFactory = $this->getMockBuilder('Magento\Quote\Model\Quote\Address\RateResult\MethodFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->rateResultMethod = $this->getMockBuilder('Magento\Quote\Model\Quote\Address\RateResult\Method')
            ->disableOriginalConstructor()
            ->getMock();
        $this->airmeeClient = $this->getMockBuilder('Airmee\Shipping\Model\AirmeeClient')
            ->disableOriginalConstructor()
            ->getMock();
        $this->deliveryCacheFactory = $this->getMockBuilder('Airmee\Shipping\Model\DeliveryCacheFactory')
            ->disableOriginalConstructor()
            ->getMock();
        $this->countryFactory = $this->getMockBuilder('Magento\Directory\Model\CountryFactory')
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFactory = $this->getMockBuilder('Airmee\Shipping\Model\Resource\DeliveryCache\CollectionFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->rateResultFactory->expects($this->any())->method('create')->willReturn($this->rateResult);
        $this->rateMethodFactory->expects($this->any())->method('create')->willReturn($this->rateResultMethod);

        $this->carrier = new Carrier(
            $this->scopeConfig,
            $this->rateErrorFactory,
            $this->logger,
            $this->rateResultFactory,
            $this->rateMethodFactory,
            $this->airmeeClient,
            $this->deliveryCacheFactory,
            $this->countryFactory,
            $this->collectionFactory,
            array()
        );
    }

    /**
     * Test getAllowedMethods
     */
    public function testGetAllowedMethods() {
        $name = 'Airmee Shipping';
        $code = 'airmee';
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn($name);
        $methods = $this->carrier->getAllowedMethods();
        $this->assertArrayHasKey($code, $methods);
        $this->assertEquals($name, $methods[$code]);
    }

    /**
     * Test collectRates with the shipping method disabled.
     */
    public function testCollectRatesNotActive()
    {
        /** @var \Magento\Quote\Model\Quote\Address\RateRequest $request */
        $request = $this->getMockBuilder('Magento\Quote\Model\Quote\Address\RateRequest')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockIsActive(false);
        $this->assertFalse($this->carrier->collectRates($request));
    }


    /**
     * Toggle configuration value for enabling/disabling the module.
     *
     * @param $result
     */
    protected function mockIsActive($result)
    {
        $this->scopeConfig
            ->expects($this->at(0))
            ->method('getValue')
            ->with('carriers/storepickup/active')
            ->willReturn($result);
    }


}